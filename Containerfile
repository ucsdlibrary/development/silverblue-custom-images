# see https://quay.io/organization/fedora-ostree-desktops
# image options: sway-atomic(sway), kinoite(kde), silverblue(gnome)
ARG SOURCE_IMAGE="${SOURCE_IMAGE:-silverblue}"
ARG SOURCE_ORG="${SOURCE_ORG:-fedora-ostree-desktops}"
ARG BASE_IMAGE="quay.io/${SOURCE_ORG}/${SOURCE_IMAGE}"

ARG FEDORA_MAJOR_VERSION="${FEDORA_MAJOR_VERSION:-41}"

# FROM golang:1.22.5-alpine3.20 AS key-fetcher
# RUN apk add curl
# RUN go install github.com/theupdateframework/go-tuf/cmd/tuf-client@latest && \
#     curl -o sigstore-root.json https://raw.githubusercontent.com/sigstore/root-signing/main/ceremony/2022-10-18/repository/5.root.json && \
#     tuf-client init https://tuf-repo-cdn.sigstore.dev sigstore-root.json
#
# RUN tuf-client get https://tuf-repo-cdn.sigstore.dev fulcio_v1.crt.pem > fulcio_v1.crt.pem
# RUN tuf-client get https://tuf-repo-cdn.sigstore.dev rekor.pub > rekor.pub

# Location not final and subject to change!
FROM ${BASE_IMAGE}:${FEDORA_MAJOR_VERSION} AS production
ARG EXTRA_PACKAGES="neovim"
ARG PACKAGES_TO_REMOVE="firefox firefox-langpacks"

LABEL org.opencontainers.image.title="Fedora Silverblue Atomic"
LABEL org.opencontainers.image.description="Customized image of Fedora Silverblue Atomic"
LABEL org.opencontainers.image.source="https://gitlab.com/ucsdlibrary/development/silverblue-custom-images"
LABEL org.opencontainers.image.licenses="MIT"

RUN dnf remove -y $PACKAGES_TO_REMOVE &&\
    dnf install -y \
    distrobox \
    git \
    iwd \
    zsh \
    $EXTRA_PACKAGES \
    && dnf clean all

# Copy custom config to /usr & /etc
COPY usr usr
COPY etc/systemd /etc/systemd
# COPY etc etc TEMPORARY to just test the image out
# COPY --from=key-fetcher /go/fulcio_v1.crt.pem /etc/pki/fulcio_v1.crt.pem
# COPY --from=key-fetcher /go/rekor.pub /etc/pki/rekor.pub

# Setup rpm-ostree and flatpak updates
RUN sed -i 's/#AutomaticUpdatePolicy.*/AutomaticUpdatePolicy=check/' /etc/rpm-ostreed.conf && \
	systemctl enable rpm-ostreed-automatic.timer && \
  systemctl enable flatpak-automatic.timer

RUN ostree container commit
