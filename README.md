# Fedora Silverblue Custom Images

These are designed to be up-to-date [Fedora Silverblue][silverblue] images that can be used for development (or other)
use cases.

From the base images, we can make additions such as setting up `podman` to alias to `docker` and installing
`docker-compose` so that within `toolbx` and/or `distrobox` images one can run our development projects using
`docker-compose` as expected.

## How to use

- Install Fedora Silverblue flavor of choice. For now let's assume the sway/sway-atomic version.
- Allow `rpm-ostree` to reference our custom silverblue images

```sh
# Configure registry to get sigstore signatures
$ sudo mkdir -p /etc/containers/registries.d/
$ sudo curl --output-dir /etc/containers/registries.d -O "https://gitlab.com/ucsdlibrary/development/silverblue-custom-images/-/raw/trunk/etc/containers/registries.d/gitlab.com-ucsdlibrary-fedora-sway.yaml"
$ sudo restorecon -RFv /etc/containers/registries.d/gitlab.com-ucsdlibrary-fedora-sway.yaml

# Setup the updated policy to prefer our gitlab registry
$ sudo curl --output-dir /etc/containers -O "https://gitlab.com/ucsdlibrary/development/silverblue-custom-images/-/raw/trunk/etc/containers/policy.json"
# Add sigstore/fucio public key and rekor public key
$ mkdir -p /etc/pki
$ sudo curl --output-dir /etc/pki -O "https://raw.githubusercontent.com/sigstore/sigstore/main/pkg/tuf/repository/targets/fulcio_v1.crt.pem"
$ sudo curl --output-dir /etc/pki -O "https://raw.githubusercontent.com/sigstore/sigstore/main/pkg/tuf/repository/targets/rekor.pub"
```

- Rebase to use our sway/sway-atomic image:

```sh
$ rpm-ostree rebase ostree-image-signed:registry:registry.gitlab.com/ucsdlibrary/development/silverblue-custom-images/sway-atomic:stable
$ rpm-ostree update
```

- Rebase without signature verification (UNTIL VERIFICATION WORKS) to sway image

see: https://coreos.github.io/rpm-ostree/container/#url-format-for-ostree-native-containers

```sh
$ rpm-ostree rebase ostree-unverified-registry:registry.gitlab.com/ucsdlibrary/development/silverblue-custom-images/sway-atomic:stable
$ rpm-ostree update
```

